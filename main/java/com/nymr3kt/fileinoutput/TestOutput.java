/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nymr3kt.fileinoutput;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author nymr3kt
 */
public class TestOutput {
    public static void main(String[] args) {
        FileOutputStream fos = null;
        ObjectOutputStream oos =null ;
        try {
            Dog dog = new Dog("Deng",2);
            Rectangle rectangle = new Rectangle(6,5);
            File file = new File("dog.obj") ;
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(dog);
            oos.writeObject(rectangle);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TestOutput.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("File Not Found !!!!");
        } catch (IOException ex) {
            Logger.getLogger(TestOutput.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(TestOutput.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }
}
