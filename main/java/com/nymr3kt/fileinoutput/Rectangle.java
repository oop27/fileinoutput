/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nymr3kt.fileinoutput;

import java.io.Serializable;

/**
 *
 * @author nymr3kt
 */
public class Rectangle implements Serializable {

    private int wide;
    private int hight;

    public Rectangle(int wide, int hight) {
        this.wide = wide;
        this.hight = hight;
    }

    public int getWide() {
        return wide;
    }

    public int getHight() {
        return hight;
    }

    public void setWide(int wide) {
        this.wide = wide;
    }

    public void setHight(int hight) {
        this.hight = hight;
    }

    @Override
    public String toString() {
        return "Rectangle{" + "wide=" + wide + ", hight=" + hight + '}';
    }

}
