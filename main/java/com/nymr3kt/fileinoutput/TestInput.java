/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nymr3kt.fileinoutput;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author nymr3kt
 */
public class TestInput {
    public static void main(String[] args) {
        FileInputStream fis = null ;
        Dog dog = null ;
        Rectangle rec = null ;
        
         ObjectInputStream ois =null;
        try {
            File file = new File("dog.obj");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            
           dog =  (Dog)ois.readObject();
           rec = (Rectangle)ois.readObject();
            System.out.println(dog);
            System.out.println(rec);
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TestInput.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TestInput.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TestInput.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fis.close();
            } catch (IOException ex) {
                Logger.getLogger(TestInput.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }
}
